#!/bin/bash

if [ -d "/var/lib/resol" ]
then
    echo "--> reŠOL už je nainstalovaný"
    exit 1
fi

# OVĚŘENÍ NAINSTALOVANÝCH PROGRAMŮ
if ! command -v cargo &> /dev/null
then
    echo "--> Cargo není nainstalovaný"
    exit 1
fi

if ! command -v tor &> /dev/null
then
    echo "--> Tor není nainstalovaný"
    exit 1
fi

if ! command -v redis-server &> /dev/null
then
    echo "--> Redis není nainstalovaný"
    exit 1
fi

# NAKLONOVAT FRONTEND REPO DO ./web/
echo "===== Klonuji frontend repo do ./web složky ====="
if [ ! -d ./web ]
then
  git clone https://codeberg.org/resol/frontend.git web || exit 1
fi

# KOMPILACE WEB SERVERU
echo "===== Kompiluji reŠOL server (tento proces může trvat několik minut) ====="
cargo build --release || exit 1

# KONFIGURACE TOR PROXY
echo "===== Vytvářím konfigurační soubory pro Tor ====="
target/release/re-sol -c 10 || exit 1

echo "===== Zapínám Redis ====="
sudo systemctl enable --now redis || exit 1

# 
echo "===== Vytvářím nového uživatele ====="
sudo useradd -m -d /var/lib/resol resol
sudo chown -R resol:resol /var/lib/resol || exit 1

# INSTALACE reŠOL SERVERU
echo "===== Instaluji reŠOL ====="
echo "==> Instaluji reŠOL soubory"

mkdir -p /tmp/resol/web
cp -r ./web/* /tmp/resol/web &> /dev/null
cp -r ./tor-group /tmp/resol/tor-group
cp ./target/release/re-sol /tmp/resol/re-sol

chmod 777 /tmp/resol/re-sol || exit 1

sudo su resol -c "cp -r /tmp/resol/* ~/" || exit 1

echo "==> Instaluji SystemD service"
sudo cp resol.service /etc/systemd/system/resol.service || exit 1
sudo systemctl enable --now resol || exit 1

echo "===== Čištění ====="
rm -rf /tmp/resol

echo "===== Instalace dokončena ====="
echo "reŠOL server byl úspěšně nainstalovaný. Momentálně běží na http://0.0.0.0:3000."