import { exit } from 'process';
import { createClient } from 'redis';
import { State, infoLogger } from './index';
import { createHash } from 'crypto';
import { universalAuth } from './api/v1/createRequest';

export let redisClient: any;

export interface cacheDetails {
  cacheQuery: string,
  expiration?: number, 
}

export async function disconnectRedis() {
  await redisClient.quit();
}

export async function connectRedis() {
  const host = State.config.redis.host || "127.0.0.1"
  const port = State.config.redis.port || 6379

  infoLogger.info("REDIS: Connecting..")

  redisClient = createClient({
    socket: {
      host: host,
      port: port,
    },
    username: State.config.redis.user ? State.config.redis.user : null,
    password: State.config.redis.password ? State.config.redis.password : null
  })

  redisClient.on('error', (err: any) => {
    infoLogger.error(`REDIS: Failed to connect to ${host}:${port}: ${err}`)
    exit(1);
  });

  await redisClient.connect();
  
  infoLogger.info("REDIS: Successfully connected")
}

export function validateRedis() {
  return State.config.redis.redis ? true : false
}

export function computeSHA256(string: string) {
  return createHash("sha256").update(string).digest("hex")
}

export async function setCache(value: string, details: cacheDetails, auth: universalAuth) {
  if (!validateRedis()) return;

  if(!details.expiration) {
    details.expiration = 31536000;
  }

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl!)
  const cacheQuery = computeSHA256(details.cacheQuery)

  // console.log(`saved to ${username}-${password}-${solUrl}-${cacheQuery} with ${details.expiration} seconds`)

  redisClient.set(
    `${username}-${password}-${solUrl}-${cacheQuery}`,
    JSON.stringify(value),
    { EX: details.expiration }
  )
}

export async function getCache(details: cacheDetails, auth: universalAuth) {
  if (!validateRedis()) return null;

  const username = computeSHA256(auth.username.toLowerCase())
  const password = computeSHA256(auth.password)
  const solUrl = computeSHA256(auth.solUrl!)
  const cacheQuery = computeSHA256(details.cacheQuery)
 
  const data = redisClient.get(
    `${username}-${password}-${solUrl}-${cacheQuery}`,
  )

  // console.log(`get from ${username}-${password}-${solUrl}-${cacheQuery}`)
  // console.log(await data)

  return (await data ? await data : null)
}
