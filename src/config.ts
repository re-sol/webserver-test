import commandExists from "command-exists";
import { State, infoLogger } from ".";
import { checkTorMasterProxy, startTor } from "./tor";
import { connectRedis } from "./cache";
import { watchTS } from "./tsWatcher";
import fs from "fs";
import path from "path";
import YAML from "yaml";
import { exit } from "process";

function warnConfigMissing(category: string) {
  infoLogger.warn(`CONFIG: '${category}' category is missing in the 'config.yaml'. Please consider removing the config to regenerate it or merging the current config file with the example config file.`)
}

function createConfig() {
  try {
    fs.writeFileSync(path.join(__dirname, "../config.yaml"), fs.readFileSync(path.join(__dirname, "../config.yaml.example")));
  } catch(err) {
    infoLogger.error(`CONFIG: Failed to create 'config.yaml': ${err}`)
  } finally {
    infoLogger.info("CONFIG: Created the 'config.yaml' file")
  }
}

export function parseConfig() {
  // create a new config if it already doesn't exist
  if (!fs.existsSync(path.join(__dirname, "../config.yaml")) && fs.existsSync(path.join(__dirname, "../config.yaml.example"))) createConfig()

  // parse config
  try {
    const configFile = fs.readFileSync(path.join(__dirname, "../config.yaml"), "utf-8");
    State.config = YAML.parse(configFile)
  } catch(err) {
    infoLogger.error(`Failed to read or parse the \`config.json\`:\n${err}`)
    exit(1)
  }
}

export function checkConfig() {
  if (State.config.server) {
    State.serverHost = State.config.server.host ? State.config.server.host : "0.0.0.0"
    State.serverPort = State.config.server.port ? State.config.server.port : 3009
  } else {
    warnConfigMissing("server")
  }

  if (!State.config.tor) {
    State.config.tor = {}
    warnConfigMissing("tor")
  }

  if (!State.config.caching) {
    State.config.caching = {}
    warnConfigMissing("caching")
  }

  if (!State.config.redis) {
    State.config.redis = {}
    warnConfigMissing("redis")
  }

  if (process.env.REDIS_HOST) {
    State.config.redis.host = process.env.REDIS_HOST
    State.config.redis.redis = true
  }

  if (process.env.REDIS_PORT) State.config.redis.port = process.env.REDIS_PORT
}

export async function configureServer() {
  // connect to Redis
  if (State.config.redis != null && State.config.redis.redis) {
    await connectRedis()
  }

  // check instance info
  if (State.config.instanceName && State.config.instanceName !== "") State.instanceName = State.config.instanceName
  if (State.config.instanceDescription && State.config.instanceDescription !== "") State.instanceDescription = State.config.instanceDescription

  if (!State.config.tor.disableTor) {
    // check Tor master proxy
    if(State.config.tor != null && State.config.tor.pingUrl != false) await checkTorMasterProxy()

    // start extra Tor proxies
    if (State.config.tor.proxyAmount > 0) {
      infoLogger.info(`TOR: Starting ${State.config.tor.proxyAmount} Tor proxies in the background..`)

      commandExists("tor")!.then(async () => {
        await startTor(State.config.tor.proxyAmount)
      }).catch(() => {
        infoLogger.error(`TOR: Couldn't start any extra proxies because the 'tor' binary doesn't exist`)

        // don't use extra proxies if they aren't loaded
        State.config.tor.proxyAmount = 0;
      })
    }
  }

  // auto compile web scripts TS
  if (State.config.compileWebTs) {
    commandExists("tsc")!.then(() => {
      watchTS();
    }).catch(() => {})
  }
}
