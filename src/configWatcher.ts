import chokidar from "chokidar"
import fs from "fs"
import { State, infoLogger } from ".";
import YAML from "yaml";
import path from "path";
import { connectRedis, disconnectRedis } from "./cache";

function deepEqual(x: any, y: any): boolean {
  return (x && y && typeof x === 'object' && typeof y === 'object') ?
    (Object.keys(x).length === Object.keys(y).length) &&
      Object.keys(x).reduce(function(isEqual, key) {
        return isEqual && deepEqual(x[key], y[key]);
      }, true) : (x === y);
}

export async function watchConfig() {
  const watcher = chokidar.watch("config.yaml")
  infoLogger.info("CONFIG: Live reload enabled.")

  watcher.on("change", async () => {
    try {
      const configFile = fs.readFileSync(path.join(__dirname, "../config.yaml"), "utf-8");
      const configTmp = await YAML.parse(configFile)

      if (State.config.redis != null && configTmp.redis != null && !deepEqual(State.config.redis, configTmp.redis)) {
        await disconnectRedis();
        await connectRedis();
      }

      // if the current loaded instance name is different than in the config
      if (configTmp.instanceName && configTmp.instanceName !== "" && State.instanceName != configTmp.instanceName) {
        State.instanceName = configTmp.instanceName;
      }
      
      if (configTmp.instanceDescription && configTmp.instanceDescription !== "" && State.instanceDescription != configTmp.instanceDescription) {
        State.instanceDescription = configTmp.instanceDescription;
      }

      State.config = configTmp 

      infoLogger.info("CONFIG: Reloaded successfully.")
    } catch(err) {
      infoLogger.error(`Failed to read or parse the \`config.json\`:\n${err}`)
    }
  })
}
