import chokidar from "chokidar"
import { infoLogger } from "."
import shell from "shelljs";

export async function watchTS() {
  const watcher = chokidar.watch("web/scripts/**/*.ts")

  watcher.on("change", () => {
    infoLogger.info(`Compiling TypeScript..`)
    compileTS();
  })
}

async function compileTS() {
  shell.exec("scripts/recompile-ts.sh", { silent: true }, (code, stdout) => {
    if (code == 0) {
      infoLogger.info(`TypeScript compiled successfully!`)
    } else {
      infoLogger.error(`Failed to compile TypeScript:\n${stdout}`)
    } 
  })
}
