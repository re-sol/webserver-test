import { Response } from "express";

export function getUserInfo(res: Response) {
    return res.send({
        "userUID": "B000/B000000",
        "organizationID": "B000",
        "personID": "B000000",
        "campaignCategoryCode": null,
        "class": {
            "id": "B000000",
            "abbrev": "I. A",
            "name": "I. A"
        },
        "fullName": "Jan Novák",
        "firstName": "Jan",
        "lastName": "Novák",
        "detailName": "žák",
        "personsInitials": "JN",
        "schoolOrganizationName": "1. základní škola Praha",
        "schoolType": "ss",
        "userType": "student",
        "userTypeText": "Žák (I. A)",
        "studyYear": 1,
        "children": [],
        "classTeacherClasses": [],
        "deputyClassTeacherClasses": [],
        "installType": "sol",
        "language": "cs",
        "schoolYearId": "B0000",
        "enabledModules": [
            {
                "module": {
                    "moduleId": "api_dom_ukoly_zak",
                    "moduleName": "Úkoly"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_hodn_predmet_zak",
                    "moduleName": "Hodnocení - žáci/studenti"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_hodnoceni_zak",
                    "moduleName": "Hodnocení - žáci/studenti"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_chovani_zak",
                    "moduleName": "Hodnocení - žáci/studenti"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_rozvrh_zak",
                    "moduleName": "Standardní formuláře - studenti, rodiče"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_vysvedceni_zak",
                    "moduleName": "Hodnocení - žáci/studenti"
                },
                "rights": []
            },
            {
                "module": {
                    "moduleId": "api_zpravy_zak",
                    "moduleName": "Zprávy - přijaté a odeslané - studenti, rodiče"
                },
                "rights": []
            }
        ]
    })
}
