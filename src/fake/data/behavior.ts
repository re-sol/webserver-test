import { Response } from "express";

export function getBehaviorData(res: Response) {
    return res.send({
        "behaviors": [
            {
                "behaviorId": "B000000",
                "behaviorReason": "Nema sesit",
                "createId": "B000000",
                "createName": "Pavel Novak",
                "date": "1970-01-01T00:00:00",
                "kindOfBehaviorAbbrev": "NEP",
                "kindOfBehaviorName": "Nepřipravenost na hodinu",
                "kindOfBehaviorsId": "esaijsfie-fs-af-eas-faiehruertf",
                "order": 1,
                "positive": null,
                "printOnReport": null,
                "referenceNumber": null,
                "schoolYearId": "B0000",
                "semesterId": "B0000",
                "signedDate": null,
                "signedId": null,
                "signedName": " ",
                "typeOfBehaviorsId": "N"
            }
        ],
        "numberProcessed": 1
    })
}
