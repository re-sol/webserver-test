import axios, { AxiosError } from "axios";
import { Request, Response } from "express";
import { SocksProxyAgent } from "socks-proxy-agent"
import { requestType } from "./requests";
import { URLSearchParams } from "url";
import { getCache, setCache } from "../../cache";
import { login } from "./types/login"
import { userInfo } from "./types/user";
import { grades } from "./types/grades";
import { grade } from "./types/grade";
import { deleteMessage, messages, setMessageRead } from "./types/messages";
import { homework } from "./types/homework";
import { attachment } from "./types/attachment";
import { timeTable } from "./types/timetable";
import { recipients } from "./types/recipients";
import { getProxy } from "../../tor";
import { behavior } from "./types/behavior";
import { checkLock, createLock, deleteLock } from "../../lock";
import { State, infoLogger } from "../..";
import { gradesFinal } from "./types/gradesFinal";
import { attachmentMessage } from "./types/messageAttachment";

export interface requestAllInformation {
  auth: universalAuth,
  request: Request,
  response: Response,
  torProxyAgent: SocksProxyAgent | null,
}

export interface universalAuth {
  username: string,
  password: string,
  solUrl?: string,
  studentId?: string | undefined,
}

interface requestDetails {
  tag: string,
}

export function getBasicAuth(request: Request): universalAuth | null {
  if (request.headers.authorization) {
    const b64auth = (request.headers.authorization || '').split(' ')[1] || ''
    const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')

    if (!login || !password) {
      throw new Error("Invalid Authorization header")
    }

    return { username: login, password: password };
  } else {
    return null
  }
}


export async function createRequest(request: Request, response: Response, details: requestDetails) {
  const torProxyAgent = State.config.tor.disableTor ? null : getProxy();
  let auth: universalAuth | null;

  try {
    const authBasic = getBasicAuth(request)

    if (authBasic) {
      auth = authBasic
    } else {
      return response.status(400).send({
        error: true,
        message: "No Authorization header"
      })
    }
  } catch(err) {
    return response.status(400).send({
      error: true,
      message: "Bad Authorization header"
    })
  }

  const oldSolUrlHeader = request.headers["sol_url"] as string
  oldSolUrlHeader ? request.headers["sol-url"] = oldSolUrlHeader : null

  const solUrlHeader = request.headers["sol-url"];
  const studentIdQuery = request.query.studentId as string

  if(!solUrlHeader) {
    return response.status(400).send({
      error: true,
      message: "No sol-url header"
    })
  }

  let token;

  let authObj: universalAuth = {
    username: auth.username,
    password: auth.password,
    solUrl: solUrlHeader as string,
    studentId: studentIdQuery ?? null 
  }

  try {
    const requestInfo: requestAllInformation = {
      auth: authObj,
      request: request,
      response: response,
      torProxyAgent: torProxyAgent ? torProxyAgent : null 
    }

    switch (details.tag) {
      case "login":
        login(requestInfo)
        break;
      case "userinfo":
        userInfo(requestInfo)
        break;
      case "grades":
        grades(requestInfo)
        break;
      case "grade":
        grade(requestInfo)
        break;
      case "gradesFinal":
        gradesFinal(requestInfo)
        break;
      case "messages":
        messages(requestInfo)
        break;
      case "messageRead":
        setMessageRead(requestInfo)
        break;
      case "messageDelete":
        deleteMessage(requestInfo)
        break;
      case "homework":
        homework(requestInfo)
        break;
      case "attachment":
        attachment(requestInfo)
        break;
      case "attachmentmessage":
        attachmentMessage(requestInfo)
        break;
      case "timetable":
        timeTable(requestInfo)
        break;
      case "recipients":
        recipients(requestInfo)
        break;
      case "behavior":
        behavior(requestInfo)
    }
  } catch (err) {
    try {
      return response.send({
        error: true,
        message: (err as AxiosError).message
      })
    } catch (_) {
      return
    }
  }

  // if(resp && resp.status == 200 && details.cache_query) {
  //   setCache(resp.data, {
  //     cacheQuery: details.cache_query,
  //     expiration: details.cache_expiration
  //   }, authObj)
  // }

  // return response.send(resp.data)
}

export async function getAccessToken(auth: any, request: Request, response: Response) {
  let sendData;

  // check for locks otherwise wait
  await checkLock(auth);
  
  try {
    const lastRefreshed = await getCache({
      cacheQuery: `last_refresh`,
    }, auth)

    const refreshToken = await getCache({
      cacheQuery: `refresh`,
    }, auth)

    const accessToken = await getCache({
      cacheQuery: `access`,
    }, auth)

    if (refreshToken != null && lastRefreshed != null && accessToken != null) {
      return accessToken.toString().replaceAll(`"`, "");
    }

    if (refreshToken != null && lastRefreshed != null && accessToken == null) {
      await createLock(auth)
      const torProxyAgent = State.config.tor.disableTor ? null : getProxy();

      const params = new URLSearchParams({
        grant_type: "refresh_token",
        refresh_token: refreshToken.toString().replaceAll(`"`, ""),
        client_id: "test_client",
        scope: "offline_access sol_api"
      })

      sendData = {
        url: `${auth.solUrl}/${requestType.Login}`,
        method: "POST",
        httpsAgent: torProxyAgent ?? null,
        httpAgent: torProxyAgent ?? null,
        data: params,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        }
      }
    } else {
      await createLock(auth)
      const torProxyAgent = State.config.tor.disableTor ? null : getProxy();

      const params = new URLSearchParams({
        grant_type: "password",
        username: auth.username,
        password: auth.password,
        client_id: "test_client",
        scope: "openid offline_access profile sol_api"
      })

      sendData = {
        url: `${auth.solUrl}/${requestType.Login}`,
        method: "POST",
        httpsAgent: torProxyAgent,
        httpAgent: torProxyAgent,
        data: params,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        }
      }
    }

    let resp;

    try {
      resp = await axios.request(sendData)
    } catch (err) {
      await deleteLock(auth)

      infoLogger.error(err)
      let message: any = { "error": true, "message": "Nepodarilo se prihlasit", error_message: null }
      if ((err as AxiosError).response && (err as AxiosError).response!.status == 400) {
        message.error_message = "Spatne uzivatelske jmeno nebo heslo"
      }
      
      return null
    }

    if (!resp.data.access_token || !resp.data.refresh_token) return null

    setCache(resp.data.access_token, {
      cacheQuery: `access`,
      expiration: 60 * 10 // 10 minutes
    }, auth)
    setCache(resp.data.refresh_token, {
      cacheQuery: `refresh`,
    }, auth)
    setCache(new Date().toUTCString(), {
      cacheQuery: `last_refresh`,
    }, auth)

    await deleteLock(auth);

    return resp.data.access_token
  } catch(err) {
    infoLogger.error(err);
    await deleteLock(auth)
  }
}
