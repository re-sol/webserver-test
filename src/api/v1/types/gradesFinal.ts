import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../../cache";
import { State } from "../../..";

export async function gradesFinal(req: requestAllInformation) {
  if (!req.request.query.studentId) {
    return req.response.status(400).send({
      error: true,
      message: "No studentId query was found in the URL query"
    })
  }
  
  const gradesFinalCache = getCache({
    cacheQuery: `grades-final-${req.request.query.studentId}`,
  }, req.auth)

  if (await gradesFinalCache) return req.response.send(await gradesFinalCache)
  let token = await getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.auth.solUrl}/${requestType.Grades}/${req.request.query.studentId}/marks/final`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    try {
      response.data["certificateTerms"].reverse(); 

      setCache(response.data, {
        cacheQuery: `grades-final-${req.request.query.studentId}`,
        expiration: State.config.caching.gradesFinal ||  60 * 10, // 10 minutes 
      }, req.auth)
      
      return req.response.send(response.data);
    } catch (err) {
      return req.response.send(response.data)
    }
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}
