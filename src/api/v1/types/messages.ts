import axios, { AxiosError, AxiosResponse } from "axios";
import { getAccessToken, requestAllInformation } from "../createRequest";
import { requestType } from "../requests";
import { getCache, setCache } from "../../../cache";
import { State } from "../../..";

export async function messages(req: requestAllInformation) {
  const messagesCache = getCache({
    cacheQuery: `messages-${req.request.query.messageStatus || "all"}-${req.request.query.pageNumber?.toString() || "1"}`,
  }, req.auth)

  if (await messagesCache) return req.response.send(await messagesCache)
  let token = await getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  let messagesQuery = new URLSearchParams({
    "Pagination.PageNumber": req.request.query.pageNumber?.toString() || "1",
    // "Pagination.PageSize": "200",
  })

  let url = requestType.Messages;

  if (req.request.query.messageStatus == "notRead") {
    messagesQuery.set("MessageReadStatus", "notRead");
  } else if (req.request.query.messageStatus == "sent") {
    url = requestType.MessagesSent;
  }

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.auth.solUrl}/${url}?${messagesQuery}`,
      method: "GET",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) {
    setCache(response.data, {
      cacheQuery: `messages-${req.request.query.messageStatus || "all"}-${req.request.query.pageNumber?.toString() || "1"}`,
      expiration: State.config.caching.messages || 60 * 10, // 10 minutes 
    }, req.auth)
    return req.response.send(response.data)
  } else {
    return req.response.status(400).send({
      error: true,
      message: "Žádné data",
    })
  }
}

export async function setMessageRead(req: requestAllInformation) {
  let token = await getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  if (!req.request.query.messageId) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: "messageId není definováno"
    })
  }

  let url = requestType.MessageRead;
  console.log(req.request.query.messageId)

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.auth.solUrl}/${url}/${req.request.query.messageId}/mark-as-read`,
      method: "PUT",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    console.log(err)
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) return req.response.send(response.data)
}

export async function deleteMessage(req: requestAllInformation) {
  let token = await getAccessToken(req.auth, req.request, req.response);
  if(!token) return

  if (!req.request.query.messageId) {
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: "messageId není definováno"
    })
  }

  let url = requestType.MessageRead;
  console.log(req.request.query.messageId)

  let response: AxiosResponse;
  try {
    response = await axios.request({
      url: `${req.auth.solUrl}/${url}/${req.request.query.messageId}`,
      method: "DELETE",
      httpsAgent: req.torProxyAgent,
      httpAgent: req.torProxyAgent,
      headers: {
        Authorization: `Bearer ${await token}`
      }
    })
  } catch(err) {
    console.log(err)
    return req.response.status(400).send({
      error: true,
      message: "Špatný request",
      specific: (err as AxiosError).message
    })
  }

  if (response.data) return req.response.send(response.data)
}

