const toggle_schedule = () => {
    const checkbox = document.getElementById("first-checkbox")! as HTMLInputElement
    const main_schedule = document.getElementById("schedule")!
    
    if (checkbox.checked) {
        main_schedule.style.display = "none";
    } else {
        main_schedule.style.display = "block";
    }
}

toggle_schedule()
