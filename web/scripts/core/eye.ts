const eyeButton = document.getElementById("eye-button")!
const eyeImg = document.getElementById("eye-img")!
const pass = document.getElementById("pass")! as HTMLInputElement

const eyeNotCrossed = document.getElementById("eye-icon-not-crossed")!;
const eyeCrossed = document.getElementById("eye-icon-crossed")!;

eyeButton.onclick = function () {
  if (pass.type == "password") {
    pass.type = "text"

    eyeNotCrossed.style.display = "block";
    eyeCrossed.style.display = "none";
  } else if (pass.type == "text") {
    pass.type = "password"

    eyeNotCrossed.style.display = "none";
    eyeCrossed.style.display = "block";
  }
};
