let date = new Date();
let today = new Date(); 

const renderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector(".days");

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();

  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  const months = [
    "Leden",
    "Únor",
    "Březen",
    "Duben",
    "Květen",
    "Červen",
    "Červenec",
    "Srpen",
    "Září",
    "Říjen",
    "Listopad",
    "Prosinec"
  ];

  
  
  


  

  let days = "";

  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class="prev-date every-date">${prevLastDay - x + 1}</div>`;
  }

  for (let i = 1; i <= lastDay; i++) {
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth()
    ) {
      days += `<div class="today calendar-date every-date">${i}</div>`;
    } else {
      days += `<div class="calendar-date every-date">${i + " "}</div>`;
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    days += `<div class="next-date every-date">${j}</div>`;
    monthDays!.innerHTML = days;
  }



  const ElementsinCalendar = document.querySelectorAll(".every-date");

  

  //changing background color
  let prevClickedElement: any = null;

  
  ElementsinCalendar.forEach(element => {
    element.addEventListener('click', () => {
      
      if (prevClickedElement) {
        prevClickedElement.classList.remove('clicked');
      }

      element.classList.add('clicked');

      prevClickedElement = element;
    });
  });
  
  const prevDate = document.getElementsByClassName("prev-date")!;
  const nextDate = document.getElementsByClassName("next-date")!;

  for (let i = 0; i < document.getElementsByClassName("calendar-date").length; i++) {
    document.getElementsByClassName("calendar-date")[i].addEventListener("click", () => {
      date.setDate(parseInt((document.getElementsByClassName("calendar-date")[i]).textContent!));
      ((document.getElementsByClassName("loading-schedule-data")!)[0] as HTMLElement).style.display = "flex";
      loadSchedule(date);
  })
  }
  for (let i = 0; i < document.getElementsByClassName("prev-date").length; i++) {
    document.getElementsByClassName("prev-date")[i].addEventListener("click", () => {
      date.setMonth(date.getMonth() - 1)
      date.setDate(Number(prevDate[i].textContent));
      ((document.getElementsByClassName("loading-schedule-data")!)[0] as HTMLElement).style.display = "flex";
      loadSchedule(date)
      date.setMonth(date.getMonth() + 1)// get current month again
    })

  }
  for (let i = 0; i < document.getElementsByClassName("next-date").length; i++) {
    document.getElementsByClassName("next-date")[i].addEventListener("click", () => {
      date.setMonth(date.getMonth() + 1)
      date.setDate(Number(nextDate[i].textContent));
      ((document.getElementsByClassName("loading-schedule-data")!)[0] as HTMLElement).style.display = "flex";
      loadSchedule(date)
      date.setMonth(date.getMonth() - 1)// get current month again
    })
  

  document.querySelector(".date h1")!.textContent = months[today.getMonth()];
  document.querySelector(".date p")!.textContent = today.getDate().toString() + "." + (today.getMonth() + 1 ).toString() + "."
  
    
}
}
renderCalendar();


//code  - https://github.com/lashaNoz/Calendar
