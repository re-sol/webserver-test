console.log('%cHold Up!', 'color: red; font-size: 50px; font-weight: bold');
console.log("%cPasting anything in here could give attackers access to your account. Make sure you know what you're doing.", 'background: #222; color: red; font-size: 20px; font-weight: bold');

if (location.protocol == "http:") {
  console.warn("This site doesn't have a SSL certificate. Logins entered here could be compromised.")
}
