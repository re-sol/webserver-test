const checkHideNext = (length: number) => {
  if (length != 30) {
    document.getElementById("load-next")!.style.display = "none";
  }
}

let lock = false;
const setupLoadNextOnScroll = async (callbackFunction: any) => {
  window.onscroll = async () => {
    if (lock) return
    if (document.getElementById("load-next")!.style.display == "none") return

    // console.log(document.getElementById("load-next")!.getBoundingClientRect().top)
    if(document.getElementById("load-next")!.getBoundingClientRect().top < 2000){
      lock = true;
      await callbackFunction(true)
      lock = false;
    }
  }
}
