const customUrl = document.getElementById("custom-url")!;
const loginUsername = document.getElementById("login-username-container")!;
const loginPassword = document.getElementById("login-password-container")!;
const solUrl = document.getElementById("sol-url")! as HTMLInputElement;

const select = (value: string) => {
  if (value == "custom") {
    customUrl.style.display = 'block';
    loginUsername.style.display = 'flex';
    loginPassword.style.display = 'flex';
  } else if (value == "https://re-sol.tech") {
    customUrl.style.display = 'none';
    loginUsername.style.display = 'none';
    loginPassword.style.display = 'none';
  } else {
    loginUsername.style.display = 'flex';
    loginPassword.style.display = 'flex';
    customUrl.style.display = 'none';
  }
}

if(solUrl.value == "custom") {
  customUrl.style.display = "block";
} else if (solUrl.value == "https://re-sol.tech") {
  loginUsername.style.display = "none";
  loginPassword.style.display = "none";
}
