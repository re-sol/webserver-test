const sleep = (milliseconds: number) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// this function realistically doesn't check only the personId
const checkStudentId = async () => {
  while (true) {
    if (localStorage.getItem("osoba_id") != null || localStorage.getItem("semester_id") != null) {
      break;
    } else {
      // console.log(localStorage.getItem("osoba_id"))
      // console.log(localStorage.getItem("semester_id"))
      await sleep(100)
    }
  }
}
