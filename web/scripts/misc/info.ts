const get_server_info = async () => {
  const response = await fetch("/api/v1/resol/info", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors",
    "headers": {
      "authorization": `Basic ${btoa(`${localStorage.getItem("username")}:${localStorage.getItem("password")}`)}`,
      "sol_url": localStorage.getItem("sol_url")!
    }
  });

  const json = await response.json();

  const instanceName = document.getElementById("instance-name");
  const instanceDescription = document.getElementById("instance-description");
  const instanceVersion = document.getElementById("instance-version");
  const instanceRedis = document.getElementById("instance-uses-redis");
  const instanceTorProxies = document.getElementById("instance-extra-tor-proxies");
  const instanceHomePage = document.getElementById("instance-homepage")!;

  const instanceServerDetails = document.getElementById("server-details");
  const instanceOSType = document.getElementById("instance-os-type");
  const instanceOSKernel = document.getElementById("instance-os-kernel");
  const instanceOSUptime = document.getElementById("instance-os-uptime");

  if (json["userAdmin"]) {
    document.getElementById("administrators")!.style.display = "block";
    document.getElementById("administrator-notice")!.style.display = "flex";
  }

  instanceName!.textContent = json["instance"]["name"]
  instanceDescription!.textContent = json["instance"]["description"]
  instanceVersion!.textContent = `Verze serveru: ${json["version"]}`
  instanceRedis!.textContent = `Používá caching: ${json["instance"]["redis"]}`
  if (json["instance"]["extraTorInstances"]) instanceTorProxies!.textContent = `Počet dodatečných Tor proxies: ${json["instance"]["extraTorInstances"]}`
  instanceHomePage.textContent = json["homepage"]

  // fill serverDetails
  if (Object.keys(json["instance"]["serverDetails"]).length != 0) {
    instanceServerDetails!.style.display = "block";
    instanceOSType!.textContent = `Typ OS: ${json["instance"]["serverDetails"]["osType"]}`
    instanceOSKernel!.textContent = `OS kernel: ${json["instance"]["serverDetails"]["osKernel"]}`
    instanceOSUptime!.textContent = `OS uptime: ${json["instance"]["serverDetails"]["osUptime"]}`
  }
  instanceHomePage.style.color = "var(--surface-1)"
  instanceHomePage.setAttribute("href", json["homepage"])
}

const getAdvancedServerInfo = async () => {
  const response = await fetch("/api/v1/resol/advancedInfo", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors"
  });

  const json = await response.json();

  if (json["error"]) return

  document.getElementById("instance-os-distro")!.textContent = `Distribuce: ${json["distro"]} ${json["osBuild"]}`
  document.getElementById("instance-os-cpuload")!.textContent = `Zatížení procesoru serveru: ${json["cpuLoad"].toFixed(2)}%`
}

const updateServer = async () => {
  document.getElementById("git-pull-text")!.textContent = "Aktualizace probíhá.."
  document.getElementById("git-pull-text")!.style.color = "var(--primary-text)"

  const response = await fetch("/api/v1/resol/update", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors",
    "headers": {
      "authorization": `Basic ${btoa(`${localStorage.getItem("username")}:${localStorage.getItem("password")}`)}`,
      "sol_url": localStorage.getItem("sol_url")!
    }
  });

  const json = await response.json()

  document.getElementById("git-pull-text")!.textContent = json["message"]
  document.getElementById("git-pull-text")!.style.color = !json["error"] ? "var(--grade-1)" : "var(--grade-5)"
}

const restartServer = async () => {
  document.getElementById("restart-server-text")!.textContent = "Verifikace.."
  document.getElementById("restart-server-text")!.style.color = "var(--primary-text)"

  const response = await fetch("/api/v1/resol/restart", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors",
    "headers": {
      "authorization": `Basic ${btoa(`${localStorage.getItem("username")}:${localStorage.getItem("password")}`)}`,
      "sol_url": localStorage.getItem("sol_url")!
    }
  });

  const json = await response.json()

  document.getElementById("restart-server-text")!.textContent = json["message"]
  document.getElementById("restart-server-text")!.style.color = !json["error"] ? "var(--grade-1)" : "var(--grade-5)"
}

const cleanAllCache = async () => {
  document.getElementById("clean-all-cache-text")!.textContent = "Mažu cache.."
  document.getElementById("clean-all-cache-text")!.style.color = "var(--primary-text)"

  const response = await fetch("/api/v1/cache/cleanAll", {
    "credentials": "include",
    "method": "GET",
    "mode": "cors",
    "headers": {
      "authorization": `Basic ${btoa(`${localStorage.getItem("username")}:${localStorage.getItem("password")}`)}`,
      "sol_url": localStorage.getItem("sol_url")!
    }
  });

  const json = await response.json()

  document.getElementById("clean-all-cache-text")!.textContent = json["message"]
  document.getElementById("clean-all-cache-text")!.style.color = !json["error"] ? "var(--grade-1)" : "var(--grade-5)"
}

let serverDetailsSSEExists: boolean = false;
const serverDetailsElement = document.getElementById("server-details") as HTMLDetailsElement

let cpuLoadChartData: number[] = []
let chart: any;

const serverDetailsSSE = (es: EventSource) => {
  const instanceOSUptime = document.getElementById("instance-os-uptime");
  let chartData = {
    type: 'line',
    // responsive: true,
    data: {
      labels: Array.from({length: 20}, () => ""),
      datasets: [{
        fill: true,
        label: 'CPU Load',
        data: cpuLoadChartData,
        borderWidth: 1,
        pointHitRadius: 60,
      }]
    },
    options: {
      scales: {
        y: {
          min: 0 
        }
      },
      animations: {
        //Boolean - If we want to override with a hard coded scale
        scaleOverride : false,
        //** Required if scaleOverride is true **
        //Number - The number of steps in a hard coded scale
        scaleSteps : 20,
        //Number - The value jump in the hard coded scale
        scaleStepWidth : 10,
        //Number - The scale starting value
        scaleStartValue : 0
      },
    }
  };

  const ctx = document.getElementById('instance-os-cpuload-chart');

  if (!chart) {
    // @ts-ignore
    chart = new Chart(ctx, chartData);
  }
   
  es.onmessage = function (event) {
    const json = JSON.parse(event.data)
    instanceOSUptime!.textContent = `OS uptime: ${json.chunk.serverDetails.osUptime}`

    const percentageCpu = json.chunk.cpuLoad.toFixed(2);

    document.getElementById("instance-os-cpuload")!.textContent = `Zatížení procesoru serveru: ${percentageCpu}%`

    if (chartData.data.datasets[0].data.length == 20) {
      chartData.data.labels = Array.from({length: 40}, () => "")
    }

    if (chartData.data.datasets[0].data.length >= 40) chartData.data.datasets[0].data.shift() 
    chartData.data.datasets[0].data.push(percentageCpu)
    chart!.update();

    chart.update();
  };
}

// EventSource or null
let sse: any = null;

serverDetailsElement.addEventListener("toggle", () => {
  if (serverDetailsElement.open) {
    // if listener already exists, exit
    if (serverDetailsSSEExists) return

    serverDetailsSSEExists = true;

    sse = new EventSource('/api/v1/sse/resol/info');
    serverDetailsSSE(sse);
  } else {
    if (sse != null) {
      sse.close()
      serverDetailsSSEExists = false;
    }
  }
})
