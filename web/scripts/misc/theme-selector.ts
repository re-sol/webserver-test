// Get theme from localStorage
const cachedTheme = localStorage.getItem("mytheme") || "default";

// Click on the radio button on page load
document.getElementById(cachedTheme)!.click();
