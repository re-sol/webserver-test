const getBehavior = async () => {
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;

  document.getElementById("dashboard-behavior-loader")!.style.display = "flex";
  const data = await fetch(`/api/v1/behavior?studentId=${studentId}`, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  const json = JSON.parse(await data.text())

  const behaviorsLength = Object.keys(json["behaviors"]).length;

  for (let i = 0; i < behaviorsLength; i++) {
    if (i > 4) break

    const currentBehavior = json["behaviors"][i]
  
    const boxCell = document.createElement("div")
    boxCell.className = "box-cell"

    const boxDate = document.createElement("p")
    boxDate.className = "box-cell-date"
    boxDate.textContent = convert_date(currentBehavior["date"])

    const boxHeading = document.createElement("div")
    boxHeading.className = "cell-name-heading";
      
    const boxCreator = document.createElement("p")
    boxCreator.className = "box-cell-sender-name";
    boxCreator.textContent = currentBehavior["createName"]
    boxCreator.style.fontWeight = "bold";

    let reason = currentBehavior["kindOfBehaviorName"];
    if (reason.length > 27) {
      reason = `${reason.slice(0, 27)}...`;
    }

    const boxReason = document.createElement("p")
    boxReason.className = "box-cell-heading";
    boxReason.textContent = reason
    const currentBehaviorPage = (behaviorPage) - 1;
    const behaviorId = currentBehavior["behaviorId"];
    boxReason.addEventListener("click", function () {
      window.location.href = `/behavior-info/${behaviorId}?page=${currentBehaviorPage}`;
    })

    boxCell.appendChild(boxDate)
    boxHeading.appendChild(boxCreator)
    boxHeading.appendChild(boxReason)
    boxCell.appendChild(boxHeading)

    const dashboardBehaviorBox = document.getElementById("dashboard-behavior")
    dashboardBehaviorBox?.appendChild(boxCell)
  }
  
  document.getElementById("dashboard-behavior-loader")!.style.display = "none";
}

let behaviorPage = 2;

const behaviorDetails = async (next: boolean) => {
  setupLoadNextOnScroll(behaviorDetails);

  document.getElementById("loading-page-info-data")!.style.display = "flex";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/behavior?studentId=${studentId}`;

  if (next) {
    url = `/api/v1/behavior?studentId=${studentId}&pageNumber=${behaviorPage}`;
    behaviorPage = behaviorPage + 1
  }

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  const json = await response.json()

  checkHideNext(Object.keys(json["behaviors"]).length)

  for (let behavior in json["behaviors"]) {
    const currentBehavior = json["behaviors"][behavior]
    createBehaviorDetail(currentBehavior);
  }

  document.getElementById("behavior-container")!.style.display = "block";
  document.getElementById("loading-page-info-data")!.style.display = "none";
}

const createBehaviorDetail = (behavior: any) => {
  const behaviorDiv = document.createElement("div");
  behaviorDiv.className = "behavior info-box";
  const behaviorId = behavior["behaviorId"]
  const behaviorClickPage = (behaviorPage) - 1

  behaviorDiv.addEventListener("click", () => {
    window.location.href = `/behavior-info/${behaviorId}?page=${behaviorClickPage}`;
  })

  const behaviorDate = document.createElement("p");
  behaviorDate.textContent = convert_date(behavior["date"])
  behaviorDate.className = "info-box-date";

  const behaviorContent = document.createElement("div");
  behaviorContent.className = "box-content"

  const behaviorInfoIcon = document.createElement("svg");
  behaviorInfoIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M20.71 7.04c.39-.39.39-1.04 0-1.41l-2.34-2.34c-.37-.39-1.02-.39-1.41 0l-1.84 1.83l3.75 3.75M3 17.25V21h3.75L17.81 9.93l-3.75-3.75L3 17.25Z"/></svg>`

  const behaviorInfo = document.createElement("div");
  behaviorInfo.className = "behavior-info"

  const behaviorTitle = document.createElement("p");
  behaviorTitle.className = "box-title";
  behaviorTitle.textContent = behavior["kindOfBehaviorName"];

  const behaviorPerson = document.createElement("p");
  behaviorPerson.className = "behavior-person";
  behaviorPerson.textContent = behavior["createName"];

  behaviorContent.appendChild(behaviorInfoIcon)
  behaviorInfo.appendChild(behaviorTitle)
  behaviorInfo.appendChild(behaviorPerson)
  behaviorContent.appendChild(behaviorInfo)
  behaviorDiv.appendChild(behaviorContent)
  behaviorDiv.appendChild(behaviorDate)
  document.getElementById("behavior-container")?.appendChild(behaviorDiv)
  document.getElementById("behavior-container")?.appendChild(document.createElement("hr"))
}

const behavior_view = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;

  const urlParams = new URLSearchParams(window.location.search);
  let behaviorPageString = "";

  if (urlParams.get("page")) behaviorPageString = `&pageNumber=${urlParams.get("page")}`
  let url = `/api/v1/behavior?studentId=${studentId}${behaviorPageString}`;

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  handle_behavior_view_data(await response.json());
}

const handle_behavior_view_data = (json: any) => {
  const last_segment_url = () => {
    return window.location.pathname.split("/").pop()
  }

  let behavior_count = Object.keys(json["behaviors"]).length;
  for (let i = 0; i < behavior_count; i++) {
    if (json["behaviors"][i]["behaviorId"] == last_segment_url()) {
      document.title = json["behaviors"][i]["kindOfBehaviorName"] + " · reŠOL";
      document.getElementById("page-info-container")!.style.display = "block";
      document.getElementById("behavior-title")!.textContent = json["behaviors"][i]["kindOfBehaviorName"];
      document.getElementById("behavior-reason")!.textContent = json["behaviors"][i]["behaviorReason"];
      document.getElementById("behavior-teacher")!.textContent = json["behaviors"][i]["createName"];
      document.getElementById("behavior-date")!.textContent = convert_date(json["behaviors"][i]["date"]);
      if (json["behaviors"][i]["signedDate"] != null) {
        document.getElementById("behavior-signed")!.textContent = json["behaviors"][i]["signedName"] + ", " + convert_date(json["behaviors"][i]["signedDate"]);
      } else {
        document.getElementById("behavior-signed")!.textContent = "Nepodepsáno";
      }

      if (json["behaviors"][i]["printOnReport"] == "true") {
        document.getElementById("behavior-print")!.textContent = "Ano";
      } else {
        document.getElementById("behavior-print")!.textContent = "Ne";
      }

      document.getElementById("loading-page-info-data")!.style.display = "none";
      return
    }
  }
  console.error("not found")
}
