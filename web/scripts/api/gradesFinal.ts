const gradesFinal = async () => {
  document.getElementById("loading-page-info-data")!.style.display = "flex";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/gradesFinal?studentId=${studentId}`;

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  const json = await response.json()
  console.log(json);
  

  let currentSchoolYear;
  let currentSchoolYearSecondSemester: boolean = false;
  for (let grade in json["certificateTerms"]) {
    const currentGrade = json["certificateTerms"][grade];

    if (currentSchoolYear == null) {
      document.getElementById("final-grade-result")!.textContent = currentGrade["achievementText"]
      document.getElementById("final-grade-timeframe")!.textContent = `${currentGrade["gradeName"]} (${currentGrade["semesterAbbrev"]})` 
      currentSchoolYear = currentGrade["schoolYearName"]
      currentSchoolYearSecondSemester = currentGrade["semesterOrder"] == 2 ? true : false

      if (currentSchoolYearSecondSemester) {
        const marksAverageSecondSemester = currentGrade["marksAverage"]
        const semester2MarksAverage = document.getElementById("semester-2-marksAverage");

        if(semester2MarksAverage && semester2MarksAverage !== null) {
          semester2MarksAverage.textContent = marksAverageSecondSemester;
          semester2MarksAverage.style.color = assign_grade_color(Number(marksAverageSecondSemester));
        }
      }

      for (let finalGrade in currentGrade["finalMarks"]) {
        const currentFinalGrade = currentGrade["finalMarks"][finalGrade]

        if (currentFinalGrade["markText"] == null) {
          continue
        }

        createGradeFinal(currentGrade, currentFinalGrade, 2) 
      }
    }

    if(currentSchoolYear == currentGrade["schoolYearName"] && currentSchoolYearSecondSemester && currentGrade["semesterOrder"] == 1) {
      const marksAverageFirstSemester =  currentGrade["marksAverage"]
      const semester1MarksAverage = document.getElementById("semester-1-marksAverage")
      
      if(semester1MarksAverage && semester1MarksAverage !== null) {
        semester1MarksAverage.textContent = marksAverageFirstSemester;
        semester1MarksAverage.style.color = assign_grade_color(Number(marksAverageFirstSemester));
      }

      for (let finalGrade in currentGrade["finalMarks"]) {
        const currentFinalGrade = currentGrade["finalMarks"][finalGrade]

        if (currentFinalGrade["markText"] == null) {
          continue
        }

        createGradeFinal(currentGrade, currentFinalGrade, 1) 
      }

      break
    }
  }

  document.getElementById("loading-page-info-data")!.style.display = "none";
}

const createGradeFinal = (currentGrade: any, currentFinalGrade: any, semester: number) => {
  const finalGradeDiv = document.createElement("div")
  finalGradeDiv.className = "final-grade"

  const finalGradeResult = document.createElement("p")
  finalGradeResult.textContent = currentFinalGrade["markText"]
  finalGradeResult.style.color = assign_grade_color(Number(currentFinalGrade["markText"]))
  finalGradeResult.style.fontWeight = "bold"; 
  finalGradeResult.className = "final-grade-result"

  let currentSubject = "Neznámý";
  for (let subject in currentGrade["subjects"]) {
    // console.log(currentGrade["subjects"][subject]["id"])
    if (currentGrade["subjects"][subject]["id"] == currentFinalGrade["subjectId"]) currentSubject = currentGrade["subjects"][subject]["name"]
  }
  const finalGradeSubject = document.createElement("p")
  finalGradeSubject.textContent = currentSubject
  finalGradeSubject.className = "final-grade-subject"

  finalGradeDiv.appendChild(finalGradeResult)
  finalGradeDiv.appendChild(finalGradeSubject)

  document.getElementById(`semester-${semester}`)?.appendChild(finalGradeDiv)
  document.getElementById(`semester-${semester}`)?.appendChild(document.createElement("hr"))
}
