const get_messages = async () => {

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  let url = "/api/v1/messages"; 

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("sol-url", sol_url);
  xmlHttp.send();

  xmlHttp.onload = () => {
    if (xmlHttp.responseText.includes("ERROR")) {
      document.getElementById("api-error")!.style.display = "flex";
      document.getElementById("api-error")!.textContent = xmlHttp.responseText;
      return;
    }

    localStorage.setItem("messages_json_cached", xmlHttp.responseText);

    let json = JSON.parse(xmlHttp.responseText);

    let messages_count = Object.keys(json["messages"]).length
    for (let i = 0; i < messages_count; i++) {
      if (i > 4){
        break
      }
      document.getElementById("message-" + i + "-date")!.textContent = convert_date(json["messages"][i]["sentDate"].split("T")[0]);
      document.getElementById("message-" + i + "-sender-name")!.innerHTML = "<b>" + json["messages"][i]["sender"]["name"] + "</b>";
      let string = json["messages"][i]["title"];
      if (string.length > 20) {
        string = `${string.slice(0, 26)}...`;
      }
      document.getElementById("message-" + i + "-heading")!.textContent = string;
      document.getElementById("message-" + i + "-heading")!.addEventListener("click", function () {
        window.location.href = `/message/${json["messages"][i]["id"]}?page=${(messagePage) - 1}`;
      });
    }
    document.getElementById("dashboard-messages-loader")!.style.display = "none";
  }
}

const message_view = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const urlParams = new URLSearchParams(window.location.search);
  let messagePageString = "";

  if (urlParams.get("page")) messagePageString = `?pageNumber=${urlParams.get("page")}`
  let url = `/api/v1/messages${messagePageString}`;

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  handle_message_view_data(await response.json());
}

const handle_message_view_data = (json: any) => {
  const last_segment_url = () => {
    return window.location.pathname.split("/").pop()
  }

  let messages_count = Object.keys(json["messages"]).length
  for (let i = 0; i < messages_count; i++) {
    if (json["messages"][i]["id"] == last_segment_url()) {
      document.title = json["messages"][i]["title"] + " · reŠOL";
      document.getElementById("message-title")!.textContent = json["messages"][i]["title"];
      document.getElementById("message-author")!.textContent = json["messages"][i]["sender"]["name"];
      document.getElementById("message-sent-date")!.textContent = convert_date_time(json["messages"][i]["sentDate"]);
      document.getElementById("message-content")!.innerHTML = json["messages"][i]["text"];
      document.getElementById("loading-page-info-data")!.style.display = "none";
      document.getElementById("message-container")!.style.display = "flex";

      if (json["messages"][i]["text"].includes("Jedná se o zprávu s přílohou. Použijte webovou aplikaci, kde si můžete přílohu zobrazit.")) document.getElementById("message-attachment-error")!.style.visibility = "visible"; 

      return
    }
  }
  console.error("not found")
}

// message overview state
let messagePage = 2;

const messagesDetails = async (next: boolean) => {
  setupLoadNextOnScroll(messagesDetails);

  document.getElementById("loading-page-info-data")!.style.display = "flex";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  let url = `/api/v1/messages`;

  if (next) {
    url = `/api/v1/messages?pageNumber=${messagePage}`;
    messagePage = messagePage + 1
  }

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  const json = await response.json()

  checkHideNext(Object.keys(json["messages"]).length)

  for (let message in json["messages"]) {
    const currentMessage = json["messages"][message]
    createMessageDetail(currentMessage);
  }

  document.getElementById("messages-container")!.style.display = "block";
  document.getElementById("loading-page-info-data")!.style.display = "none";
}

const createMessageDetail = (message: any) => {
  const messageDiv = document.createElement("div");
  messageDiv.className = "message info-box";

  const messageId = message["id"]
  const messageClickPage = (messagePage) - 1
  messageDiv.addEventListener("click", () => {
    window.location.href = `/message/${messageId}?page=${messageClickPage}`;
  })

  const messageDate = document.createElement("p");
  messageDate.className = "info-box-date";
  messageDate.textContent = convert_date(message["sentDate"])
  
  const messageContent = document.createElement("div");
  messageContent.className = "box-content"

  const messageInfoIcon = document.createElement("svg");
  messageInfoIcon.innerHTML = `<svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" viewBox="0 0 24 24"><path fill="currentColor" d="M18 8H6V6h12v2m0 3H6V9h12v2m0 3H6v-2h12v2m4-10a2 2 0 0 0-2-2H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h14l4 4V4Z"/></svg>`

  const messageInfo = document.createElement("div");
  messageInfo.className = "message-info"

  const messageTitle = document.createElement("p");
  messageTitle.className = "box-title";
  messageTitle.textContent = message["title"];

  const messagePerson = document.createElement("p");
  messagePerson.className = "message-person";
  messagePerson.textContent = message["sender"]["name"];

  messageContent.appendChild(messageInfoIcon)
  messageInfo.appendChild(messageTitle)
  messageInfo.appendChild(messagePerson)
  messageContent.appendChild(messageInfo)
  messageDiv.appendChild(messageContent)
  messageDiv.appendChild(messageDate)
  document.getElementById("messages-container")?.appendChild(messageDiv)
  document.getElementById("messages-container")?.appendChild(document.createElement("hr"))
}

get_messages()
