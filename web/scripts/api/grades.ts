const last_segment_url = () => {
  return window.location.pathname.split("/").pop()
}

const get_event_id = () => {
  return window.location.pathname.split("/")[2]
}

const get_student_id = () => {
  return window.location.pathname.split("/")[3]
}

const get_grades = async () => {
  document.getElementById("dashboard-grades-loader")!.style.display = "";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/grades?studentId=${studentId}`;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("sol-url", sol_url);

  // If subjects json or grades json not cached
  xmlHttp.send();
  xmlHttp.onload = () => {
    handle_grades_data(xmlHttp.responseText)
  }
}

const handle_grades_data = (mainResponse: string) => {
  if (mainResponse.includes("ERROR")) {
    document.getElementById("api-error")!.style.display = "flex";
    document.getElementById("api-error")!.textContent = "error occurred when fetching grades or subjects";
    return;
  }

  localStorage.setItem("grades_json_cached", mainResponse);

  let json = JSON.parse(mainResponse);

  let znamky = [];
  let prumery = [];

  let grades_count = Object.keys(json["marks"]).length
  let j = 0;
  for (let i = 0; i < grades_count; i++) {
    // console.log(`index: ${i}, j: ${j}, mark: ${json["marks"][j]["markText"]}`)
    if (i > 9){
      break
    }

    // if mark is not received yet
    if(json["marks"][j]["markText"] == "-") {
      j++;
      i--;
      continue;
    }

    document.getElementById("grade-" + i + "-date")!.textContent = json["marks"][j]["markDate"].split("T")[0];
    document.getElementById("grade-" + i + "-grade")!.textContent = json["marks"][j]["markText"];
    document.getElementById("grade-" + i + "-grade")!.style.color = assign_grade_color(Number(json["marks"][j]["markText"]));
    znamky.push(Number(json["marks"][j]["markText"]));
    prumery.push(Number(json["marks"][j]["weight"]));

    document.getElementById("grade-" + i + "-grade")!.style.cursor = "pointer";
      
    let gradeId = json["marks"][j]["id"];
    document.getElementById("grade-" + i + "-grade")!.addEventListener("click", () => {
      window.location.href = `/grade/${gradeId}`;
    })

    let subjects_count = Object.keys(json["subjects"]).length
    
    for (let x = 0; x < subjects_count; x++) {
      if(json["subjects"][x]["id"] == json["marks"][j]["subjectId"]) {
        document.getElementById("grade-" + i + "-name")!.textContent = json["subjects"][x]["abbrev"];
        break;
      }
    }

    j++
  }

  createGradeAverageWeight(znamky, prumery);

  document.getElementById("dashboard-grades-loader")!.style.display = "none";
}

const grade_view = async () => {
  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  await checkStudentId()
  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;

  const urlParams = new URLSearchParams(window.location.search);
  let gradePageString = "";

  if (urlParams.get("page")) gradePageString = `&pageNumber=${urlParams.get("page")}`
  let url = `/api/v1/grades?studentId=${studentId}${gradePageString}`;

  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open( "GET", url, true ); // false for synchronous request
  xmlHttp.setRequestHeader("Authorization", "Basic " + btoa(user + ":" + pass))
  xmlHttp.setRequestHeader("sol-url", sol_url);

  document.getElementById("loading-page-info-data")!.style.display = "flex"; // Show loader

  xmlHttp.send();

  xmlHttp.onload = () => {
  }
}


// grade overview state
let gradePage = 2;

const gradesDetails = async (next: boolean) => {
  setupLoadNextOnScroll(gradesDetails);

  document.getElementById("loading-page-info-data")!.style.display = "flex";
  await checkStudentId();

  let user = localStorage.getItem("username")
  let pass = localStorage.getItem("password")
  let sol_url: string = (localStorage.getItem("sol_url") ? localStorage.getItem("sol_url")! : "https://aplikace.skolaonline.cz");

  const studentId = localStorage.getItem("selected_zak")! || localStorage.getItem("osoba_id")!;
  let url = `/api/v1/grades?studentId=${studentId}`;

  if (next) {
    url = `/api/v1/grades?studentId=${studentId}&pageNumber=${gradePage}`;
    gradePage = gradePage + 1
  }

  const response = await fetch(url, {
    "method": "GET",
    "headers": {
      "Authorization": `Basic ${btoa(`${user}:${pass}`)}`,
      "sol-url": sol_url
    }
  })

  const json = await response.json()

  checkHideNext(Object.keys(json["marks"]).length)

  for (let markKey in json["marks"]) {
    const currentMark = json["marks"][markKey]

    const markDiv = document.createElement("div")
    markDiv.className = "grade info-box"

    const markDate = document.createElement("p");
    markDate.className = "info-box-date";
    markDate.textContent = convert_date(currentMark["editDate"])

    const markInfo = document.createElement("div")
    markInfo.className = "box-content"

    const markDescriptive = document.createElement("div")
    markDescriptive.className = "grade-descriptive"

    const mark = document.createElement("p")
    mark.className = "grade-result" 
    mark.textContent = currentMark["markText"]
    mark.style.color = assign_grade_color(Number(currentMark["markText"]))

    // get subject by id
    let subjectName = "Neznámý";
    for (let subject in json["subjects"]) {
      if (currentMark["subjectId"] == json["subjects"][subject]["id"]) {
        subjectName = json["subjects"][subject]["name"]
      }
    }

    const markSubject = document.createElement("p")
    markSubject.className = "box-title" 
    markSubject.textContent = subjectName

    const markTopic = document.createElement("p")
    markTopic.className = "grade-topic";
    markTopic.textContent = currentMark["theme"] 

    const markId = currentMark["id"] // defining this here because the next iteration will overwrite this
    markDiv.addEventListener("click", () => {
      window.location.href = `/grade/${markId}`;
    })
    

    markInfo.appendChild(mark)

    markDescriptive.appendChild(markSubject)
    markDescriptive.appendChild(markTopic)

    markInfo.appendChild(markDescriptive)

    markDiv.appendChild(markInfo)
    markDiv.appendChild(markDate)

    document.getElementById("grades-container")!.appendChild(markDiv)
    document.getElementById("grades-container")!.appendChild(document.createElement("hr"))
  }

  document.getElementById("loading-page-info-data")!.style.display = "none";
  document.getElementById("grades-container")!.style.display = "block"
}

const create_grade_average = (znamky: number[]) => {
  const average = (arr: number[]) => arr.reduce( ( p, c ) => p + c, 0 ) / arr.length;
  
  let result = average(znamky)
  
  document.getElementById("prumer-znamek")!.textContent = (result as unknown) as string;
  document.getElementById("prumer-znamek")!.style.color = assign_grade_color(result)!;
}

const createGradeAverageWeight = (znamky: number[], prumery: number[]) => {
  const weightedAverage = (nums: number[], weights: number[]) => {
    const [sum, weightSum] = weights.reduce(
      (acc, w, i) => {
        acc[0] = acc[0] + nums[i] * w;
        acc[1] = acc[1] + w;
        return acc;
      },
      [0, 0]
    );
    return sum / weightSum;
  };
  
  let result = weightedAverage(znamky, prumery).toFixed(2)
  
  document.getElementById("prumer-znamek")!.textContent = result;
  document.getElementById("prumer-znamek")!.style.color = assign_grade_color(Number(result))!;
}

const assign_grade_color = (grade: number) => {
  let grade_1_color = window.getComputedStyle(document.body).getPropertyValue('--grade-1');
  let grade_2_color = window.getComputedStyle(document.body).getPropertyValue('--grade-2');
  let grade_3_color = window.getComputedStyle(document.body).getPropertyValue('--grade-3');
  let grade_4_color = window.getComputedStyle(document.body).getPropertyValue('--grade-4');
  let grade_5_color = window.getComputedStyle(document.body).getPropertyValue('--grade-5');
  
  if(Math.round(grade) == 1) {
    return grade_1_color
  } else if(Math.round(grade) == 2) {
    return grade_2_color
  } else if(Math.round(grade) == 3) {
    return grade_3_color
  } else if(Math.round(grade) == 4) {
    return grade_4_color
  } else {
    return grade_5_color
  }
}
